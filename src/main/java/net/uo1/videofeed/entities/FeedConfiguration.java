/*
* PROPRIETARY
*/
package net.uo1.videofeed.entities;

import java.util.Collections;
import java.util.Map;

/**
 * A class representing the configuration of video feed sources.
 * It stores information like URL, fields, and translation maps for each source
 * mentioned in the JSON provided.
 * This class is used to configure various video feed sources for a system.
 */
public class FeedConfiguration {

    /**
     * Whether the source is enabled.
     */
    private boolean enabled;

    /**
     * The name of the video feed source.
     */
    private String sourceName;

    /**
     * The URL to fetch the feed from.
     */
    private String url;

    /**
     * A string containing the fields of the source feed.
     */
    private String fields;
    
    /**
     * A map of the fields that need to be translated.
     * The keys are the original field names and values are the new names.
     * For example, "Video ID" will be translated to "source_id".
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> translateFields = Collections.EMPTY_MAP;
    
    /**
     * The delimiter used to separate fields within the source feed.
     */
    private String delimiter = ",";

    /**
     * The delimiter used for separating different values within a field, for example, for list of tags.
     */
    private String listDelimiter = ";";

    /**
     * A map of field extractors to extract specific information from the field.
     * The keys are the field names and the values are the regular expression patterns used to extract the information.
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> fieldSanitizers = Collections.EMPTY_MAP;
    
    /**
     * A map of field extractors to extract specific information from the field.
     * The keys are the field names and the values are the regular expression patterns used to extract the information.
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> fieldExtractors = Collections.EMPTY_MAP;
    
    /**
     * A map of field generators that generate fields based on extracted information from the feed.
     * The keys are the field names and the values are regular expressions with named groups that match supported
     * field names. The only exception is `sourceid`, that maps to `source_id`.
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> fieldGenerators = Collections.EMPTY_MAP;

    @SuppressWarnings("unchecked")
    private Map<String, String> defaultValues = Collections.EMPTY_MAP;

    /**
     * Whether the source is enabled.
     *
     * @return true if the source is enabled, false otherwise.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets whether the source is enabled.
     *
     * @param enabled true if the source is enabled, false otherwise.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * The name of the video feed source.
     *
     * @return the name of the video feed source.
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * Sets the name of the video feed source.
     *
     * @param sourceName the name of the video feed source.
     */
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    /**
     * The URL to fetch the feed from.
     *
     * @return the URL to fetch the feed from.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the URL to fetch the feed from.
     *
     * @param url the URL to fetch the feed from.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * A string containing the fields of the source feed.
     *
     * @return a string containing the fields of the source feed.
     */
    public String getFields() {
        return fields;
    }

    /**
     * Sets the string containing the fields of the source feed.
     *
     * @param fields a string containing the fields of the source feed.
     */
    public void setFields(String fields) {
        this.fields = fields;
    }

    /**
     * A map of the fields that need to be translated.
     * The keys are the original field names and values are the new names.
     * For example, "Video ID" will be translated to "source_id".
     *
     * @return a map of the fields that need to be translated.
     */
    public Map<String, String> getTranslateFields() {
        return translateFields;
    }

    /**
     * Sets the map of the fields that need to be translated.
     * The keys are the original field names and values are the new names.
     * For example, "Video ID" will be translated to "source_id".
     *
     * @param translateFields a map of the fields that need to be translated.
     */
    public void setTranslateFields(Map<String, String> translateFields) {
        this.translateFields = translateFields;
    }

    /**
     * The delimiter used to separate fields within the source feed.
     *
     * @return the delimiter used to separate fields within the source feed.
     */
    public String getDelimiter() {
        return delimiter;
    }

    /**
     * Sets the delimiter used to separate fields within the source feed.
     *
     * @param delimiter the delimiter used to separate fields within the source feed.
     */
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    /**
     * The delimiter used for separating different values within a field, for example, for list of tags.
     *
     * @return the delimiter used for separating different values within a field.
     */
    public String getListDelimiter() {
        return listDelimiter;
    }

    /**
     * Sets the delimiter used for separating different values within a field, for example, for list of tags.
     *
     * @param listDelimiter the delimiter used for separating different values within a field.
     */
    public void setListDelimiter(String listDelimiter) {
        this.listDelimiter = listDelimiter;
    }

    /**
     * @return the fieldSanitizers
     */
    public Map<String, String> getFieldSanitizers() {
        return fieldSanitizers;
    }

    /**
     * @param fieldSanitizers the fieldSanitizers to set
     */
    public void setFieldSanitizers(Map<String, String> fieldSanitizers) {
        this.fieldSanitizers = fieldSanitizers;
    }

    /**
     * A map of field extractors to extract specific information from the field.
     * The keys are the field names and the values are the regular expression patterns used to extract the information.
     *
     * @return a map of field extractors to extract specific information from the field.
     */
    public Map<String, String> getFieldExtractors() {
        return fieldExtractors;
    }

    /**
     * Sets the map of field extractors to extract specific information from the field.
     * The keys are the field names and the values are the regular expression patterns used to extract the information.
     *
     * @param fieldExtractors a map of field extractors to extract specific information from the field.
     */
    public void setFieldExtractors(Map<String, String> fieldExtractors) {
        this.fieldExtractors = fieldExtractors;
    }

    /**
     * Retrieves the map of field generators that generate fields based on extracted information from the feed.
     * The keys are the field names and the values are regular expressions with named groups that match supported
     * field names. The only exception is `sourceid`, which maps to `source_id`.
     *
     * @return a map of field generators that generate fields based on extracted information from the feed.
     */
    public Map<String, String> getFieldGenerators() {
        return fieldGenerators;
    }

    /**
     * Sets the map of field generators that generate fields based on extracted information from the feed.
     * The keys are the field names and the values are regular expressions with named groups that match supported
     * field names. The only exception is `sourceid`, which maps to `source_id`.
     *
     * @param fieldGenerators a map of field generators that generate fields based on extracted information from the feed.
     */
    public void setFieldGenerators(Map<String, String> fieldGenerators) {
        this.fieldGenerators = fieldGenerators;
    }

    /**
     * @return the defaultValues
     */
    public Map<String, String> getDefaultValues() {
        return defaultValues;
    }

    /**
     * @param defaultValues the defaultValues to set
     */
    public void setDefaultValues(Map<String, String> defaultValues) {
        this.defaultValues = defaultValues;
    }

}
