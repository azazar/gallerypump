/*
 * PROPRIETARY
 */
package net.uo1.videofeed.entities;

import net.uo1.util.Base62;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;
import org.apache.commons.lang3.StringUtils;
import net.uo1.videofeed.collections.Facets;
import net.uo1.feedreader.GalleryValidator;
import net.uo1.feedreader.util.RawFieldUtils;
import net.uo1.util.ArrayFilter;
import static net.uo1.feedreader.util.RawFieldUtils.*;
import static net.uo1.util.SequenceUtil.*;
import static net.uo1.videofeed.collections.Facets.*;

/**
 * Represents an external gallery with various properties and methods.
 * Used for parsing and validating galleries with custom fields.
 *
 * @author Mikhail Yevchenko
 */
public class ExternalGallery {

    /**
     * Creates an ExternalGallery object from a given map and set of collectFields.
     *
     * @param feedRecord map containing gallery information
     * @param collectFields set of fields to be collected
     * @return an ExternalGallery object
     */
    public static ExternalGallery fromMap(Map<String, Object> feedRecord, Set<String> collectFields) {
        ExternalGallery r = new ExternalGallery();

        r.source = (String) feedRecord.get("source");

        r.sourceId = (String) feedRecord.get("source_id");

        r.url = (String) feedRecord.get("url");

        r.title = (String) feedRecord.get("title");

        r.description = (String) feedRecord.get("description");
        
        r.seconds = parseDurationString((String) feedRecord.get("seconds"));
        
        r.preview = (String) feedRecord.get("preview");

        r.thumbs = filterBlankAndTrimStrings(asStringArray(feedRecord.get("thumbs")));

        if (r.thumbs.length == 0) {
            var thumb = (String) feedRecord.get("thumb");
            if (thumb != null && !thumb.isEmpty()) {
                r.thumbs = new String[] { thumb };
            }
        }

        r.embed = RawFieldUtils.fixEmbed((String) feedRecord.get("embed"));

        r.orientation = (String) feedRecord.get("orientation");

        r.facets = new Facets(
            filterBlankAndTrimStrings(asStringArray(feedRecord.get(FACET_TAG))),
            filterBlankAndTrimStrings(asStringArray(feedRecord.get(FACET_CATEGORY))),
            filterBlankAndTrimStrings(asStringArray(feedRecord.get(FACET_CHANNEL))),
            filterBlankAndTrimStrings(asStringArray(feedRecord.get(FACET_MODEL))),
            filterBlankAndTrimStrings(asStringArray(feedRecord.get(FACET_STUDIO)))
        );

        r.date = null;

        if (feedRecord.containsKey("date")) {
            String dateStr = (String) feedRecord.get("date");

            if (!StringUtils.isEmpty(dateStr)) {
                for (DateFormat dateFormat : RawFieldUtils.DATE_FORMATS) {
                    try {
                        r.date = dateFormat.parse(dateStr);
                        break;
                    } catch (ParseException ex) {
                    }
                }
            }
        }

        r.quality = (String) feedRecord.get("quality");

        if (collectFields != null && !collectFields.isEmpty()) {
            for (Map.Entry<String, Object> e : feedRecord.entrySet()) {
                String key = e.getKey();

                if (key.isEmpty() || "-".equals(key)) {
                    continue;
                }

                if (!collectFields.contains(key)) {
                    continue;
                }

                if (!RawFieldUtils.KNOWN_FIELDS.contains(key)) {
                    if (r.custom == null) {
                        r.custom = new LinkedHashMap<>();
                    }

                    r.custom.put(e.getKey(), e.getValue());
                }
            }
        }

        r.repair();

        return r;
    }

    /**
     * Returns a numeric representation of the passed-in source ID.
     *
     * @param id The source ID to be converted
     * @return Numeric source ID
     */
    public static long getNumericSourceId(String id) {
        int i = id.indexOf('_');

        if (i == -1) {
            throw new IllegalStateException("id=" + id);
        }

        return Base62.decode(id.substring(i + 1));
    }

    public String source;
    public String sourceId;
    public String url;
    public String title;
    public String description;
    public Long seconds;
    public String preview;
    public String[] thumbs;
    public String embed;
    public Facets facets;
    public String orientation;
    public Date date;
    public String quality;
    public Map<String, Object> custom;

    /**
     * Repairs the title and description and filters any bad words from the facets.
     */
    public void repair() {
        title = RawFieldUtils.repairText(title);

        description = RawFieldUtils.repairText(description);
        
        facets.replaceAll((k, v) -> ArrayFilter.removeElements(v, RawFieldUtils::isBadWord));

        if (date != null && date.getTime() <= 0) {
            date = null;
        }
    }

    public StringBuilder toString(StringBuilder b) {
        b.append("  source:").append(source).append('\n');
        b.append("  sourceId:").append(sourceId).append('\n');
        b.append("  url:").append(url).append('\n');
        b.append("  title:").append(title).append('\n');
        b.append("  description:").append(description).append('\n');
        b.append("  seconds:").append(seconds).append('\n');
        b.append("  preview:").append(preview).append('\n');
        b.append("  thumbs:").append(thumbs).append('\n');
        b.append("  embed:").append(embed).append('\n');
        b.append("  orientation:").append(orientation).append('\n');
        b.append("  facets: ").append(facets).append('\n');
        b.append("  date:").append(date).append('\n');
        b.append("  quality:").append(quality).append('\n');
        if (custom != null && !custom.isEmpty()) {
            b.append("  custom fields:\n");
            for (Map.Entry<String, Object> e : custom.entrySet()) {
                b.append("    ").append(e.getKey()).append(":").append(e.getValue()).append('\n');
            }
        }

        b.append("  errors:\n");
        if (GalleryValidator.BASIC.validate(this, (f, m) -> {
            b.append(f).append(": ").append(m).append('\n');
            return false;
        })) {
            b.append("    none");
        }

        return b;
    }

    /**
     * Returns a string representation of the ExternalGallery object.
     *
     * @return a string representation of the ExternalGallery object
     */
    @Override
    public String toString() {
        return toString(new StringBuilder()).toString();
    }

    /**
     * Gets a unique identifier for the ExternalGallery object.
     *
     * @return a concatenated string of source and sourceIdEncoded
     */
    public String getId() {
        String sourceIdEncoded;

        try {
            sourceIdEncoded = StringUtils.isNumeric(sourceId) ? Base62.encode(Long.parseLong(sourceId)) : sourceId;
        } catch (NumberFormatException ex) {
            sourceIdEncoded = sourceId;
        }

        return source == null || source.isEmpty() ? sourceIdEncoded : source + "_" + sourceIdEncoded;
    }

}
