/*
 * PROPRIETARY
 */
package net.uo1.videofeed.collections;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Mikhail Yevchenko
 */
public class Facets extends AbstractMap<String, String[]> {

    public static final String FACET_TAG = "tags";
    public static final String FACET_CATEGORY = "categories";
    public static final String FACET_CHANNEL = "channels";
    public static final String FACET_MODEL = "models";
    public static final String FACET_STUDIO = "studios";
    
    private static final String[] FACET_NAMES = {FACET_TAG, FACET_CATEGORY, FACET_CHANNEL, FACET_MODEL, FACET_STUDIO};

    private static final int NUM_FACETS = FACET_NAMES.length;

    public static final List<String> FACETS = List.of(FACET_NAMES);

    public static boolean isFacet(String name) {
        return switch (name) {
            case FACET_TAG, FACET_CATEGORY, FACET_CHANNEL, FACET_MODEL, FACET_STUDIO-> true;
            default -> false;
        };
    }
    
    private static String[] EMPTY = new String[0];

    public String[] tags;
    public String[] categories;
    public String[] channels;
    public String[] models;
    public String[] studios;

    public Facets(String[] tags, String[] categories, String[] channels, String[] models, String[] studios) {
        this.tags = tags;
        this.categories = categories;
        this.channels = channels;
        this.models = models;
        this.studios = studios;
    }

    public Facets() {
        tags = EMPTY;
        categories = EMPTY;
        channels = EMPTY;
        models = EMPTY;
        studios = EMPTY;
    }

    @Override
    public boolean containsKey(Object key) {
        return isFacet((String)key);
    }

    @Override
    public String[] get(Object key) {
        return switch ((String)key) {
            case FACET_TAG -> tags;
            case FACET_CATEGORY -> categories;
            case FACET_CHANNEL -> channels;
            case FACET_MODEL -> models;
            case FACET_STUDIO -> studios;
            default -> null;
        };
    }

    @Override
    public String[] put(String key, String[] value) {
        switch (key) {
            case FACET_TAG: tags = value; break;
            case FACET_CATEGORY: categories = value; break;
            case FACET_CHANNEL: channels = value; break;
            case FACET_MODEL: models = value; break;
            case FACET_STUDIO: studios = value; break;
            default: throw new IllegalArgumentException(key);
        }
        
        return EMPTY;
    }

    @Override
    public int size() {
        return NUM_FACETS;
    }

    @Override
    public Set<String> keySet() {
        return Set.of(FACET_NAMES);
    }

    @Override
    public Collection<String[]> values() {
        return List.of(tags, categories, channels, models, studios);
    }

    private static abstract class FacetEntry implements Map.Entry<String, String[]> {
        
        public String key;

        public FacetEntry(String key) {
            this.key = key;
        }

        @Override
        public String getKey() { return key; }

        @Override
        public abstract String[] getValue();

        @Override
        public String[] setValue(String[] value) { setValueSimple(value); return EMPTY; }
        
        public abstract void setValueSimple(String[] value);
        
    }

    @Override
    public Set<Map.Entry<String, String[]>> entrySet() {
        return Set.of(
            new FacetEntry(FACET_TAG) {
                @Override public String[] getValue() { return tags; }
                @Override public void setValueSimple(String[] value) { tags = value; }
            },
            new FacetEntry(FACET_CATEGORY) {
                @Override public String[] getValue() { return categories; }
                @Override public void setValueSimple(String[] value) { categories = value; }
            },
            new FacetEntry(FACET_CHANNEL) {
                @Override public String[] getValue() { return channels; }
                @Override public void setValueSimple(String[] value) { channels = value; }
            },
            new FacetEntry(FACET_MODEL) {
                @Override public String[] getValue() { return models; }
                @Override public void setValueSimple(String[] value) { models = value; }
            },
            new FacetEntry(FACET_STUDIO) {
                @Override public String[] getValue() { return studios; }
                @Override public void setValueSimple(String[] value) { studios = value; }
            }
        );
    }

}
