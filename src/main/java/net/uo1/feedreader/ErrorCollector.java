/*
 * PROPRIETARY
 */
package net.uo1.feedreader;

/**
 * Interface for collecting errors.
 *
 * @author Mikhail Yevchenko
 */
public interface ErrorCollector {

    /**
     * Collects error message for a given field.
     *
     * @param field The field name.
     * @param message The error message.
     * @return true if the error is successfully collected, false otherwise.
     */
    boolean collect(String field, String message);

    /**
     * Collects an error message for a given field with default error message "bad value".
     *
     * @param field The field name.
     * @return true if the error is successfully collected, false otherwise.
     */
    default boolean collect(String field) {
        return collect(field, "bad value");
    }

    /**
     * Collects an error message for a given field with error message "value missing".
     *
     * @param field The field name.
     * @return true if the error is successfully collected, false otherwise.
     */
    default boolean empty(String field) {
        return collect(field, "value missing");
    }

    /**
     * Collects an error message for a given field with error message "illegal value".
     *
     * @param field The field name.
     * @return true if the error is successfully collected, false otherwise.
     */
    default boolean illegal(String field) {
        return collect(field, "illegal value");
    }

}