/*
 * PROPRIETARY
 */
package net.uo1.feedreader;

import com.univocity.parsers.common.*;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.uo1.feedreader.util.ValueExtractor;
import net.uo1.util.StringValueSanitizer;
import net.uo1.util.TemplateStringBuilder;
import net.uo1.videofeed.entities.FeedConfiguration;
import org.apache.commons.lang3.ArrayUtils;

/**
 * This class provides an implementation for StreamFeed to read records from CSV and TSV files.
 * @author Mikhail Yevchenko
 */
public class SVFeed extends StreamFeed {

    private static final Logger LOG = Logger.getLogger(SVFeed.class.getName());

    private Consumer<Map<String, String>> rowReadCallback;

    protected AbstractParser<?> parser = null;

    /**
     * Initializes SVFeed with the source key, feed configuration, collect fields, and size limit.
     * 
     * @param sourceKey The key to identify the source of the feed.
     * @param feedConfiguration The FeedConfiguration object to initialize the feed.
     * @param collectFields The set of fields to process and store.
     * @param sizeLimit The maximum size of the feed to process.
     * @throws IOException If an input/output exception occurs.
     */
    public SVFeed(String sourceKey, FeedConfiguration feedConfiguration, Set<String> collectFields,
            int sizeLimit) throws IOException {
        super(sourceKey, feedConfiguration, collectFields, sizeLimit);
    }

    private <T extends CommonParserSettings<?>> T updateParserSettings(T parserSettings) {
        
        String[] fields = feedConfiguration.getFields() == null ? null : ((String) feedConfiguration.getFields()).split(",");

        if (fields != null) {
            parserSettings.setHeaders(fields);
        } else {
            parserSettings.setHeaderExtractionEnabled(true);
        }

        parserSettings.setMaxCharsPerColumn(1024 * 64);
        parserSettings.setInputBufferSize(1024 * 128);

        parserSettings.setCommentProcessingEnabled(false);

        return parserSettings;
    }

    @Override
    protected void initializeStream() {
        String delimiter = feedConfiguration.getDelimiter();

        switch (delimiter) {

            case ",": {
                var settings = new CsvParserSettings();
                settings.setQuoteDetectionEnabled(true);
                parser = new CsvParser(updateParserSettings(settings));
                break;
            }
            case "\t": {
                var settings = new TsvParserSettings();
                parser = new TsvParser(updateParserSettings(settings));
                break;
            }
            default: {
                CsvParserSettings settings = new CsvParserSettings();
                settings.getFormat().setDelimiter(delimiter);
                settings.setQuoteDetectionEnabled(true);
                parser = new CsvParser(updateParserSettings(settings));
                break;
            }
        }

        parser.beginParsing(inputStream, StandardCharsets.UTF_8);
    }

    /**
     * Reads records from a CSV or TSV file and returns them as a map.
     * 
     * @return A map containing field names as keys and their values.
     * @throws IOException If an input/output exception occurs.
     */
    @Override
    public Map<String, Object> readInternal() throws IOException {
        Map<String, String> row;
        Map<String, String> row_;

        for (;;) {
            if (parser == null) {
                if (!switchToNextPath()) {
                    return null;
                }
            }

            Record record;
            String[] fields = null;

            for (;;) {
                try {
                    record = parser.parseNextRecord();
                    break;
                } catch (TextParsingException ex) {
                    if (ex.getCause() instanceof IOException) {
                        throw (IOException) ex.getCause();
                    }

                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            
            if (fields == null) {
                fields = parser.getContext().headers();
                int nullIndex = ArrayUtils.indexOf(fields, null);
                
                if (nullIndex != -1)
                    fields = Arrays.copyOf(fields, nullIndex);
            }

            if (record != null) {
                row_ = record.toFieldMap(fields);

                if (rowReadCallback != null) {
                    rowReadCallback.accept(row_);
                }

                break;
            }

            parser.stopParsing();
            parser = null;
        }

        row = row_;

        for(var e : row_.entrySet()) {
            e.setValue(StringValueSanitizer.sanitize(e.getValue()));
        }

        if (!fieldSanitizers.isEmpty()) {
            for(var fieldSanitizer : fieldSanitizers.entrySet()) {
                row_.compute(fieldSanitizer.getKey(), (key, value) -> {
                    if (value == null) {
                        return null;
                    }

                    var sanitizer = fieldSanitizer.getValue();

                    var valueStr = String.valueOf(value);

                    var matcher = sanitizer.matcher(valueStr);

                    return matcher.replaceAll("");
                });
            }
        }

        Map<String, Object> resultRow = new AbstractMap<String, Object>() {
            @Override
            public boolean containsKey(Object key) {
                return row.containsKey(translateFieldName((String) key));
            }

            @Override
            public Object get(Object key) {
                return translateFieldValue((String) key, row.get(key));
            }

            @Override
            public Set<Map.Entry<String, Object>> entrySet() {
                Set<Map.Entry<String, String>> entrySet = row.entrySet();

                return new AbstractSet<Map.Entry<String, Object>>() {
                    @Override
                    public Iterator<Map.Entry<String, Object>> iterator() {
                        Iterator<Map.Entry<String, String>> it = entrySet.iterator();

                        return new Iterator<Entry<String, Object>>() {
                            @Override
                            public boolean hasNext() {
                                return it.hasNext();
                            }

                            @Override
                            public Map.Entry<String, Object> next() {
                                Map.Entry<String, String> next = it.next();

                                if (next == null) {
                                    return null;
                                }

                                String key = translateFieldName(next.getKey());

                                return new Entry<String, Object>() {
                                    @Override
                                    public String getKey() {
                                        return key;
                                    }

                                    @Override
                                    public Object getValue() {
                                        Object value = next.getValue();

                                        if (value == null || value.toString().isEmpty()) {
                                            return null;
                                        }

                                        return translateFieldValue(key, next.getValue());
                                    }

                                    @Override
                                    public Object setValue(Object value) {
                                        if (value instanceof String stringValue) {
                                            return translateFieldValue(key, row.put(key, stringValue));
                                        } else if (value instanceof List listValue) {
                                            StringBuilder b = new StringBuilder();

                                            for (int i = 0; i < listValue.size(); i++) {
                                                if (i > 0) {
                                                    b.append(feedConfiguration.getListDelimiter());
                                                }
                                                b.append(listValue.get(i));
                                            }
                                            return translateFieldValue(key, row.put(key, b.toString()));
                                        } else {
                                            throw new UnsupportedOperationException("Not supported yet.");
                                        }
                                    }
                                };
                            }
                        };
                    }

                    @Override
                    public int size() {
                        return entrySet.size();
                    }
                };
            }

        };

        if (!fieldExtractors.isEmpty()) {
            resultRow = new LinkedHashMap<>(resultRow);

            for (ValueExtractor fieldExtractor : fieldExtractors) {
                fieldExtractor.extract(resultRow, n -> n.equals("sourceid") ? "source_id" : n);
            }
        }

        if (!fieldGenerators.isEmpty()) {
            if (!(resultRow instanceof LinkedHashMap)) {
                resultRow = new LinkedHashMap<>(resultRow);
            }

            for (Map.Entry<String, TemplateStringBuilder> e : fieldGenerators.entrySet()) {
                resultRow.put(e.getKey(), e.getValue().build(resultRow));
            }
        }

        if (!feedConfiguration.getDefaultValues().isEmpty()) {
            LinkedHashMap<String, Object> out = null;

            for (var e : feedConfiguration.getDefaultValues().entrySet()) {
                if (resultRow.get(e.getKey()) == null) {
                    if (out == null) {
                        if (!(resultRow instanceof LinkedHashMap)) {
                            resultRow = new LinkedHashMap<>(resultRow);
                        }
                        
                        out = (LinkedHashMap<String, Object>) resultRow;
                    }
                    
                    out.put(e.getKey(), e.getValue());
                }
            }
        }

        return resultRow;
    }

    /**
     * @return the rowReadCallback
     */
    Consumer<Map<String, String>> getRowReadCallback() {
        return rowReadCallback;
    }

    /**
     * @param rowReadCallback the rowReadCallback to set
     */
    void setRowReadCallback(Consumer<Map<String, String>> rowReadCallback) {
        this.rowReadCallback = rowReadCallback;
    }

}
