/*
 * PROPRIETARY
 */
package net.uo1.feedreader.util;

import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.uo1.util.PatternUtil;

/**
 *
 * @author Mikhail Yevchenko
 */
public class ValueExtractor {

    private final String source;
    private final String[] targets;
    private final Pattern regExp;

    public ValueExtractor(String source, String regExp) {
        this(source, PatternUtil.getPatternNamedGroups(regExp).toArray(new String[1]), Pattern.compile(regExp));
    }

    public ValueExtractor(String source, String[] targets, Pattern regExp) {
        this.source = source;
        this.targets = targets;
        this.regExp = regExp;
    }

    public void extract(Map<String, Object> storage, Function<String, String> nameMapper) {
        Object sourceValue = storage.get(source);

        if (sourceValue == null) {
            return;
        }

        String value = sourceValue.toString();

        Matcher matcher = regExp.matcher(value);

        if (matcher.find()) {
            for (String target : targets) {
                String group = matcher.group(target);

                if (group != null && group.length() > 0) {
                    storage.put(nameMapper.apply(target), group);
                }
            }
        }
    }

}
