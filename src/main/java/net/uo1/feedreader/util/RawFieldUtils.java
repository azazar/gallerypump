/*
 * PROPRIETARY
 */
package net.uo1.feedreader.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.text.StringEscapeUtils;

/**
 *
 * @author Mikhail Yevchenko
 */
public class RawFieldUtils {

    private static final Pattern[] SECONDS_REGEXPS = new Pattern[]{
        Pattern.compile("^(\\d+)(?: +sec(?:onds)?)?$"),};

    private static final Pattern[] MIN_SEC_REGEXPS = new Pattern[]{
        Pattern.compile("^(\\d+ *(?:min(?:utes?))) *(\\d+)(?: +sec(?:onds)?)?$"),
        Pattern.compile("^(\\d+):(\\d+)$"),};

    private static final Pattern[] HOUR_MIN_SEC_REGEXPS = new Pattern[]{
        Pattern.compile("^(\\d+):(\\d+):(\\d+)$"),};

    public static Set<String> KNOWN_FIELDS = new HashSet<>(Arrays.asList(new String[]{"source", "source_id", "url", "title", "description", "seconds", "thumbs", "embed", "orientation", "categories", "tags", "channels", "models", "studios", "date"}));

    public static Set<String> NONFACET_LIST_FIELDS = new HashSet<>(Arrays.asList(new String[] {"thumbs"}));

    public static final DateFormat[] DATE_FORMATS = new DateFormat[]{utcSimpleDate("yyyy-MM-dd HH:mm:ss"), utcSimpleDate("yyyy-MM-dd"), utcSimpleDate("yyyy.MM.dd"), utcSimpleDate("dd.MM.yyyy")};

    public static final List<DateFormat> ALLOWED_DATE_FORMATS = Collections.unmodifiableList(Arrays.asList(DATE_FORMATS));

    private static final Pattern[] STRING_CLEANERS = new Pattern[]{Pattern.compile("(?:(?:https?:\\/\\/)\\S+|www\\.[\\S]+|(?:[A-Za-z0-9_-]+\\.)+[A-Za-z]{2,})")};

    private static final Pattern HTML_ENTITY_REGEXP = Pattern.compile("&([^_]+)_");

    public static boolean isBadWord(String word) {
        return switch(word.toLowerCase()) {
            case "unknown", "none", "null", "empty" -> true;
            default -> false;
        };
    }

    public static Long parseDurationString(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        }

        for (Pattern pattern : SECONDS_REGEXPS) {
            Matcher m = pattern.matcher(s);

            if (m.find()) {
                return Long.parseLong(m.group(1));
            }
        }

        for (Pattern pattern : MIN_SEC_REGEXPS) {
            Matcher m = pattern.matcher(s);

            if (m.find()) {
                return Long.parseLong(m.group(1)) * 60 + Long.parseLong(m.group(2));
            }
        }

        for (Pattern pattern : HOUR_MIN_SEC_REGEXPS) {
            Matcher m = pattern.matcher(s);

            if (m.find()) {
                return (Long.parseLong(m.group(2)) * 60 * 60) + (Long.parseLong(m.group(2)) * 60) + Long.parseLong(m.group(3));
            }
        }

        return null;
    }

    public static String removeStreamProtocols(String pathname) {
        if (pathname.startsWith("zip://%f#")) {
            return pathname.substring("zip://%f#".length());
        }
        
        return pathname;
    }

    public static String cleanText(String s) {
        s = HTML_ENTITY_REGEXP.matcher(s).replaceAll((r) -> StringEscapeUtils.unescapeHtml4("&"+r.group(1)+";"));

        for (Pattern cleaner : STRING_CLEANERS) {
            s = cleaner.matcher(s).replaceAll("");
        }

        return s;
    }

    private static SimpleDateFormat utcSimpleDate(String fmt) {
        SimpleDateFormat f = new SimpleDateFormat(fmt);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        return f;
    }

    public static String fixEmbed(String embed) {
        if (embed == null || embed.isEmpty()) {
            return null;
        }
        if (embed.startsWith("http:") || embed.startsWith("https:")) {
            return "<iframe src=\"" + embed + "\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>";
        }
        return embed;
    }

    public static String repairText(String text) {
        if (text == null || text.isEmpty()) {
            return "";
        }

        text = RawFieldUtils.cleanText(text);
        text = text.trim();

        return text;
    }

    private RawFieldUtils() {
    }

}
