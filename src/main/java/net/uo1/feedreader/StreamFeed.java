/*
 * PROPRIETARY
 */
package net.uo1.feedreader;

import net.uo1.feedreader.util.RawFieldUtils;
import net.uo1.feedreader.util.ValueExtractor;
import net.uo1.io.ObjectReader;
import net.uo1.videofeed.collections.Facets;
import net.uo1.videofeed.entities.ExternalGallery;
import net.uo1.videofeed.entities.FeedConfiguration;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import net.uo1.util.UrlStreamOpener;
import net.uo1.util.TemplateStringBuilder;
import org.apache.commons.lang3.StringUtils;
import static net.uo1.util.CollectionUtil.isEmpty;

/**
 * StreamFeed is a class that reads an external feed and returns the data as an ExternalGallery object.
 * 
 * @author Mikhail Yevchenko
 */
public abstract class StreamFeed implements ObjectReader<ExternalGallery> {

    private static final Logger LOG = Logger.getLogger(StreamFeed.class.getName());

    public final String sourceKey;
    protected final FeedConfiguration feedConfiguration;

    protected InputStream inputStream = null;

    protected int remainingLimit;

    protected List<String> paths;

    protected Set<String> collectFields;
    protected Map<String, Pattern> fieldSanitizers;
    protected Set<ValueExtractor> fieldExtractors;
    protected Map<String, TemplateStringBuilder> fieldGenerators;

    protected GalleryValidator galleryValidator = GalleryValidator.BASIC;
    private ConcurrentHashMap<String, AtomicInteger> errors = new ConcurrentHashMap<>();
    private int invalid = 0;

    /**
     * Constructs a new StreamFeed object with the given source key, feed configuration, set of collect fields, and size limit.
     * 
     * @param sourceKey The source key for the feed.
     * @param feedConfiguration The feed configuration.
     * @param collectFields The set of fields to collect from the feed.
     * @param sizeLimit The maximum number of objects to read from the feed.
     * @throws IOException If an I/O error occurs.
     */
    protected StreamFeed(String sourceKey, FeedConfiguration feedConfiguration, Set<String> collectFields, int sizeLimit) throws IOException {
        this.sourceKey = sourceKey;
        this.feedConfiguration = feedConfiguration;
        this.collectFields = collectFields;

        paths = List.of(feedConfiguration.getUrl());

        if (isEmpty(feedConfiguration.getFieldSanitizers())) {
            fieldSanitizers = Collections.emptyMap();
        }
        else {
            fieldSanitizers = new LinkedHashMap<>();

            for(var entry : feedConfiguration.getFieldSanitizers().entrySet()) {
                fieldSanitizers.put(entry.getKey(), Pattern.compile(entry.getValue()));
            }
        }

        if (isEmpty(feedConfiguration.getFieldExtractors())) {
            fieldExtractors = Collections.emptySet();
        }
        else {
            fieldExtractors = new LinkedHashSet<>();

            for (Map.Entry<String, String> e : feedConfiguration.getFieldExtractors().entrySet()) {
                fieldExtractors.add(new ValueExtractor(e.getKey(), e.getValue()));
            }
        }

        if (isEmpty(feedConfiguration.getFieldGenerators())) {
            fieldGenerators = Collections.emptyMap();
        }
        else {
            fieldGenerators = new LinkedHashMap<>();

            for (Map.Entry<String, String> e : feedConfiguration.getFieldGenerators().entrySet()) {
                fieldGenerators.put(e.getKey(), TemplateStringBuilder.create(e.getValue()));
            }
        }

        this.remainingLimit = sizeLimit;
    }

    /**
     * Returns the GalleryValidator used to validate galleries.
     * 
     * @return The GalleryValidator used to validate galleries.
     */
    public GalleryValidator getGalleryValidator() {
        return galleryValidator;
    }

    /**
     * Sets the GalleryValidator used to validate galleries.
     * 
     * @param galleryValidator The GalleryValidator used to validate galleries.
     */
    public void setGalleryValidator(GalleryValidator galleryValidator) {
        this.galleryValidator = galleryValidator;
    }

    /**
     * Returns the source key for the feed.
     * 
     * @return The source key for the feed.
     */
    public String getSourceKey() {
        return sourceKey;
    }

    protected boolean switchToNextPath() throws IOException {
        finalizeStream();
        close();

        if (paths.isEmpty()) {
            return false;
        }

        String file = paths.get(0);
        
        paths = paths.subList(1, paths.size());

        file = file.replace("{limit}", remainingLimit > 0 ? ((Number)remainingLimit).toString() : "2147483647");

        LOG.log(Level.FINE, "Loading {0}", file);

        inputStream = UrlStreamOpener.openUrl(file);
        
        initializeStream();

        return true;
    }

    protected void initializeStream() throws IOException {
    }

    protected void finalizeStream() throws IOException {
    }

    @Override
    public void close() throws IOException {
        if (inputStream != null) {
            inputStream.close();
            inputStream = null;
        }
    }

    protected String translateFieldName(String key) {
        var fieldNameMap = feedConfiguration.getTranslateFields();

        if (fieldNameMap != null) {
            String translatedKey = fieldNameMap.get(key);
            if (translatedKey != null) {
                return translatedKey;
            }
        }

        if (key.length() >= 3 && key.charAt(key.length() - 3) == '_') {
            String keyName = key.substring(0, key.length() - 3);

            if (fieldNameMap != null) {
                String translatedKey = fieldNameMap.get(keyName);
                if (translatedKey != null) {
                    return translatedKey + key.substring(key.length() - 3, key.length());
                }
            }
        }

        return key;
    }

    protected Object translateFieldValue(String fieldName, String rawValue) {
        fieldName = translateFieldName(fieldName);

        if (rawValue == null) {
            return null;
        }

        if (Facets.isFacet(fieldName) || RawFieldUtils.NONFACET_LIST_FIELDS.contains(fieldName)) {
            return StringUtils.split(rawValue, feedConfiguration.getListDelimiter());
        }

        return rawValue;
    }

    /**
     * Returns the number of invalid galleries encountered while reading the feed.
     * 
     * @return The number of invalid galleries encountered while reading the feed.
     */
    public int getInvalid() {
        return invalid;
    }

    @Override
    public final ExternalGallery read() throws IOException {
        if (remainingLimit == 0) {
            return null;
        }

        Map<String, Object> raw;

        for (;;) {
            raw = readInternal();

            if (raw == null) {
                return null;
            }

            Map<String, Object> result = new LinkedHashMap<>(raw);

            result.put("source", getSourceKey());

            try {
                ExternalGallery vi;

                try {
                    vi = ExternalGallery.fromMap(result, collectFields);
                } catch (RuntimeException ex) {
                    LOG.log(Level.WARNING, String.valueOf(result), ex);
                    continue;
                }

                if (vi == null || !galleryValidator.validate(vi, (f, e) -> {
                    errors.computeIfAbsent(f + ": " + e, n -> new AtomicInteger()).incrementAndGet();
                    return false;
                })) {
                    invalid++;
                    continue;
                }

                remainingLimit--;

                return vi;
            } catch (IllegalStateException ex) {
                LOG.log(Level.INFO, null, ex);
            }
        }
    }

    protected abstract Map<String, Object> readInternal() throws IOException;

}
