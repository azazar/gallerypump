/*
 * PROPRIETARY
 */
package net.uo1.feedreader;

import net.uo1.videofeed.entities.ExternalGallery;

/**
 * Interface for validating external galleries.
 * 
 * @author Mikhail Yevchenko
 */
public interface GalleryValidator {

    public static final GalleryValidator BASIC = (ExternalGallery gallery, ErrorCollector errorCollector) -> {
        if ((gallery.sourceId == null || gallery.sourceId.isEmpty()) && !errorCollector.empty("sourceId")) return false;

        return true;
    };

    public static final GalleryValidator COMPLETE = (ExternalGallery gallery, ErrorCollector errorCollector) -> {
        if (!BASIC.validate(gallery, errorCollector)) return false;

        if ((gallery.title == null || gallery.title.isEmpty()) && !errorCollector.empty("title")) return false;
        if ((gallery.thumbs == null || gallery.thumbs.length == 0) && !errorCollector.empty("thumbs")) return false;

        if (gallery.url == null && !errorCollector.empty("url")) return false;
        if (gallery.seconds == null && !errorCollector.empty("seconds")) return false;
        if ((gallery.embed == null || gallery.embed.isEmpty()) && !errorCollector.empty("embed")) return false;

        int facets = 0;

        for (var facet : gallery.facets.values()) {
            facets += facet.length;
        }

        if (facets == 0 && !errorCollector.empty("facets")) return false;

        if (gallery.date == null && !errorCollector.empty("date")) return false;

        return true;
    };

    /**
     * Validates an ExternalGallery object against a set of criteria.
     * 
     * @param gallery The ExternalGallery to validate.
     * @param errorCollector An ErrorCollector to log errors.
     * @return true if the ExternalGallery is valid, false otherwise.
     */
    boolean validate(ExternalGallery gallery, ErrorCollector errorCollector);

    /**
     * Checks if an ExternalGallery object is valid.
     * 
     * @param gallery The ExternalGallery to check.
     * @return true if the ExternalGallery is valid, false otherwise.
     */
    default boolean isValid(ExternalGallery gallery) {
        return validate(gallery, (f, e) -> false);
    }

}
