/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Mikhail Yevchenko
 */
class InputStreamWrapper extends InputStream {
    
    protected InputStream is;

    @Override
    public int read() throws IOException {
        return is.read();
    }

    @Override
    public boolean markSupported() {
        return is.markSupported();
    }

    @Override
    public synchronized void reset() throws IOException {
        is.reset();
    }

    @Override
    public synchronized void mark(int readlimit) {
        is.mark(readlimit);
    }

    @Override
    public void close() throws IOException {
        is.close();
    }

    @Override
    public int available() throws IOException {
        return is.available();
    }

    @Override
    public void skipNBytes(long n) throws IOException {
        is.skipNBytes(n);
    }

    @Override
    public long skip(long n) throws IOException {
        return is.skip(n);
    }

    @Override
    public int readNBytes(byte[] b, int off, int len) throws IOException {
        return is.readNBytes(b, off, len);
    }

    @Override
    public byte[] readNBytes(int len) throws IOException {
        return is.readNBytes(len);
    }

    @Override
    public byte[] readAllBytes() throws IOException {
        return is.readAllBytes();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return is.read(b, off, len);
    }

    @Override
    public int read(byte[] b) throws IOException {
        return is.read(b);
    }
    
}
