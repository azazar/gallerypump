/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.Arrays;
import java.util.function.Predicate;

/**
 *
 * @author Mikhail Yevchenko
 */
public class ArrayFilter {

    public static <T> T[] removeElements(T[] array, Predicate<T> filter) {
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (filter.test(array[i])) {
                count++;
            } else if (count > 0) {
                array[i - count] = array[i];
            }
        }

        if (count == 0) {
            // No modifications, return the original array
            return array;
        }

        return Arrays.copyOf(array, array.length - count);
    }

    private ArrayFilter() {
    }

}
