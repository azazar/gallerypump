/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.*;
import java.util.stream.Stream;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Mikhail Yevchenko
 */
public class SequenceUtil {
    
    private static final String[] EMPTY = new String[0];

    public static List<?> asList(Object sequence) {
        if (sequence == null) {
            return Collections.emptyList();
        }

        if (sequence instanceof List list) {
            return list;
        }

        if (sequence instanceof Object[] objectArray) {
            return Arrays.asList(objectArray);
        }

        if (sequence instanceof Collection collection) {
            return Arrays.asList(collection.toArray());
        }

        if (sequence instanceof Stream stream) {
            return stream.toList();
        }

        if (sequence instanceof String string) {
            String[] strings = StringUtils.split(string, ',');
            
            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].trim();

            }
            
            return Arrays.asList(strings);
        }

        throw new UnsupportedOperationException(sequence.getClass().getCanonicalName());
    }

    public static String[] asStringArray(Object sequence) {
        if (sequence == null) {
            return EMPTY;
        }
        
        if (sequence instanceof String[] stringArray) {
            return stringArray;
        }
        
        return asList(sequence).toArray(String[]::new);
    }

    public static String[] filterBlankAndTrimStrings(String[] strings) {
        int l = 0;
        for(int i = 0; i < strings.length; i++) {
            strings[l] = strings[i].trim();

            if (!strings[l].isBlank()) {
                l++;
            }
        }

        if (l == strings.length)
            return strings;

        return ArrayUtils.subarray(strings, 0, l);
    }

    private SequenceUtil() {
    }

}
