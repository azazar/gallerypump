/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.*;

/**
 *
 * @author Mikhail Yevchenko
 */
public class TemplateStringBuilder {

    public static TemplateStringBuilder create(String template) {
        List<TemplatePart> parts = new ArrayList<>();

        int i;

        while ((i = template.indexOf('{')) != -1) {
            if (i > 0) {
                String text = template.substring(0, i);

                parts.add((sb, ctx) -> sb.append(text));
            }

            int e = template.indexOf('}', i + 1);

            if (e == -1) {
                throw new IllegalArgumentException(template);
            }

            String key = template.substring(i + 1, e).trim();

            parts.add((sb, ctx) -> {
                Object value = ctx.get(key);
                if (value != null) {
                    sb.append(value);
                }
            });

            template = template.substring(e + 1);
        }

        String lastPart = template;

        if (lastPart.length() > 0) {
            parts.add((sb, ctx) -> sb.append(lastPart));
        }

        return new TemplateStringBuilder(parts.toArray(new TemplatePart[0]));
    }

    private final TemplatePart[] template;

    private TemplateStringBuilder(TemplatePart[] template) {
        this.template = template;
    }

    public String build(Map<String, Object> context) {
        StringBuilder sb = new StringBuilder();

        for (TemplatePart part : template) {
            part.generate(sb, context);
        }

        return sb.toString();
    }

    private static interface TemplatePart {

        void generate(StringBuilder sb, Map<String, Object> context);

    }

}
