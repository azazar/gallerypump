/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Mikhail Yevchenko
 */
public class PatternUtil {

    private static final Pattern NG_PATTERN = Pattern.compile("\\(\\?(?:P?<([^>]+)>|'([^']+)')");

    public static Set<String> getPatternNamedGroups(Collection<String> patterns) {
        Set<String> result = new LinkedHashSet<>();
        
        for (String pattern : patterns) {
            getPatternNamedGroups(pattern, result);
        }
        
        return result;
    }

    public static Set<String> getPatternNamedGroups(String pattern) {
        Set<String> result = new LinkedHashSet<>();
        
        getPatternNamedGroups(pattern, result);
        
        return result;
    }

    private static void getPatternNamedGroups(String pattern, Set<String> result) {

        Matcher m = NG_PATTERN.matcher(pattern);

        while (m.find()) {
            for (String groupName : new String[]{m.group(1), m.group(2)}) {
                if (groupName != null && groupName.length() > 0) {
                    result.add(groupName);
                }
            }
        }
    }

}
