/*
 * PROPRIETARY/CONFIDENTIAL
 */
package net.uo1.util;

import java.util.Map;

/**
 *
 * @author Mikhail Yevchenko <m.ṥῥẚɱ.ѓѐḿởύḙ@uo1.net>
 */
public class CollectionUtil {

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

}
