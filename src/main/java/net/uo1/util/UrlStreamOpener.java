/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.zstandard.ZstdCompressorInputStream;


/**
 *
 * @author Mikhail Yevchenko
 */
public class UrlStreamOpener {
    
    public static String USER_AGENT = "GalleryPump/1.0 (https://gitlab.com/azazar/gallerypump; Java)";

    public static Consumer<HttpURLConnection> CONNECTION_INITIALIZER = conn -> {
        conn.setConnectTimeout(60000);
        conn.setReadTimeout(600000);
        conn.setInstanceFollowRedirects(true);
    };
    
    public static InputStream openUrl(String url) throws IOException {
        return openUrl(url, null);
    }
    
    @SuppressWarnings("resource")
    private static InputStream openUrl(String url, InputStream context) throws IOException {
        int colonIdx = url.indexOf(':');
        
        if (colonIdx == -1)
            return context == null ? new FileInputStream(url) : context;
        
        var protocol = url.substring(0, colonIdx);
        var path = url.substring(colonIdx + 1);
        
        if ("tgz".equals(protocol)) {
            protocol = "tar";
            path = "gz:" + path;
        }

        return switch (protocol) {
            case "gz" -> new GZIPInputStream(openUrl(path));
            case "zst", "zstd" -> new ZstdCompressorInputStream(openUrl(path));
            case "zip" -> openArchive(path, context, UrlStreamOpener::openZip);
            case "tar" -> openArchive(path, context, UrlStreamOpener::openTar);
            default -> openNetworkUrl(url);
        };
    }
    
    private static InputStream openNetworkUrl(String url) throws IOException {
        var conn = new URL(url).openConnection();
        
        if (conn instanceof HttpURLConnection httpConn) {
            httpConn.setRequestProperty("User-Agent", USER_AGENT);
            CONNECTION_INITIALIZER.accept(httpConn);
        }
        
        return conn.getInputStream();
    }
    
    private interface ArchiveFileOpener {
        
        InputStream open(InputStream is, String filename) throws IOException;
        
    }
    
    private static InputStream openArchive(String path, InputStream context, ArchiveFileOpener opener) throws IOException {
        if (context == null) {
            int splitterIndex = path.indexOf('!');
            
            if (splitterIndex == -1) {
                context = openUrl(path);
                path = null;
            }
            else {
                context = openUrl(path.substring(0, splitterIndex));
                path = path.substring(splitterIndex + 1);
                
                if (path.isEmpty())
                    path = null;
            }
        }
        
        return opener.open(context, path);
    }

    private static InputStream openTar(InputStream context, String filename) throws IOException {
        TarArchiveInputStream zis = new TarArchiveInputStream(context);
        var close = true;

        try {
            ArchiveEntry entry;
            String name = null;

            while((entry = zis.getNextEntry()) != null) {
                name = entry.getName();

                if (filename == null || filename.equals(name)) {
                    close = false;

                    return zis;
                }
            }

            if (name == null)
                throw new FileNotFoundException("Empty zip file");

            throw new FileNotFoundException("File \"" + filename + "\" not found, but there was \"" + name + "\"");
        }
        finally {
            if (close) {
                zis.close();
            }
        }
    }
    
    private static InputStream openZip(InputStream context, String filename) throws IOException {
        ZipInputStream zis = new ZipInputStream(context);
        var close = true;
        
        try {
            ZipEntry entry;
            String name = null;

            while((entry = zis.getNextEntry()) != null) {
                name = entry.getName();

                if (filename == null || filename.equals(name)) {
                    close = false;
                    return zis;
                }
            }

            if (name == null)
                throw new FileNotFoundException("Empty zip file");

            throw new FileNotFoundException("File \"" + filename + "\" not found, but there was \"" + name + "\"");
        }
        finally {
            if (close) {
                zis.close();
            }
        }
    }

}
