/*
 * PROPRIETARY/CONFIDENTIAL
 */
package net.uo1.util;

/**
 *
 * @author Mikhail Yevchenko <spam@uo1.net>
 */
public class StringValueSanitizer {

    public static String sanitize(String s) {
        if (s == null) {
            return null;
        }

        if (s.startsWith("\"") && s.endsWith("\"")) {
            return s.substring(1, s.length() - 1).replace("\"\"", "\"");
        }
        
        if (s.startsWith("'") && s.endsWith("'")) {
            return s.substring(1, s.length() - 1).replace("''", "'");
        }

        switch(s.toLowerCase()) {
            case "":
            case "null":
            case "none":
            case "nan":
            case "no":
            case "-":
                return null;
            default:
                return s;
        }
    }

}
