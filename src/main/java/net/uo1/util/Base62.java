/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.Arrays;

/**
 *
 * @author Mikhail Yevchenko
 */
public class Base62 {

    private static final char[] B62 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
    private static final char B62_MAX, B62_MIN;
    private static final byte[] B62_REVERSE;

    static {
        B62_REVERSE = new byte[0x80];
        Arrays.fill(B62_REVERSE, (byte) -1);

        char min = 0;
        char max = 0x7f;

        for (byte i = 0; i < B62.length; i++) {
            char ch = B62[i];
            if (ch < min) {
                min = ch;
            }
            if (ch > max) {
                max = ch;
            }
        }

        B62_MIN = min;
        B62_MAX = max;

        for (byte i = 0; i < B62.length; i++) {
            char ch = B62[i];

            B62_REVERSE[(byte) ch - B62_MIN] = i;
        }

    }

    public static String encode(long v) {
        StringBuilder b = new StringBuilder();

        encode(v, b);

        return b.toString();
    }

    public static void encode(long v, StringBuilder b) {
        while (v < 0) {
            b.append(B62[(int) Long.remainderUnsigned(v, 62)]);
            v = Long.divideUnsigned(v, 62);
        }

        while (v > 0) {
            b.append(B62[(int) (v % 62)]);
            v /= 62;
        }
    }

    public static long decode(CharSequence str) {
        long number = 0;

        for (int i = str.length() - 1; i >= 0; i--) {
            char c = str.charAt(i);

            if (c < B62_MIN || c > B62_MAX) {
                throw new IllegalArgumentException(str.toString());
            }

            int value = B62_REVERSE[c - B62_MIN];

            if (value == -1) {
                throw new IllegalArgumentException(str.toString() + " (" + c + ':' + i + ')');
            }

            number = (number * 62) + value;
        }

        return number;
    }

}
