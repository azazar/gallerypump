/*
 * PROPRIETARY
 */
package net.uo1.io;

import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 * Represents a reader that reads objects of a specific type from an underlying source.
 * This reader is closeable and iterable, and can be used with a foreach loop.
 *
 * @author Mikhail Yevchenko
 * @param <T> the type of objects to be read
 */
public interface ObjectReader<T> extends Closeable, Iterable<T> {

    /**
     * Reads an object from the underlying source.
     *
     * @return the read object or null if the end of the source is reached
     * @throws java.io.IOException if an I/O error occurs while reading the object
     */
    T read() throws IOException;

    @Override
    default Iterator<T> iterator() {
        return new Iterator<T>() {

            T current = null;
            T next;

            {
                next();
            }

            @Override
            public boolean hasNext() {
                return next != null;
            }

            @Override
            public T next() {
                try {
                    current = next;
                    next = read();

                    return current;
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
    }

    @Override
    default void forEach(Consumer<? super T> action) {
        try {
            T value;

            while ((value = read()) != null) {
                action.accept(value);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}
