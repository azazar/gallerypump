/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.Random;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Mikhail Yevchenko
 */
public class Base62Test {

    @Test
    public void base62Test() {
        assertEquals(1, Base62.decode(Base62.encode(1)));
        assertEquals(26, Base62.decode(Base62.encode(26)));
        assertEquals(27, Base62.decode(Base62.encode(27)));
        assertEquals(28, Base62.decode(Base62.encode(28)));

        for (int i = 0; i < 1000; i++) {
            assertEquals(i, Base62.decode(Base62.encode(i)));
        }

        Random r = new Random(0);

        for (int i = 0; i < 10000; i++) {
            long v = r.nextLong() << 1;

            assertEquals(v, Base62.decode(Base62.encode(v)));
        }
    }

}
