/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.LinkedHashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Mikhail Yevchenko
 */
public class TemplateStringBuilderTest {

    Map<String, Object> testData = new LinkedHashMap<String, Object>() {
        {
            put("s1", "a");
            put("s2", "b");
            put("s3", "c");
            put("i1", 1);
            put("i2", 2);
            put("i3", 3);
        }
    };

    public TemplateStringBuilderTest() {
    }

    public void testTemplate(String expected, String template) {
        TemplateStringBuilder st = TemplateStringBuilder.create(template);

        assertEquals(expected, st.build(testData));
    }

    @Test
    public void testBuild() {
        testTemplate("123", "123");
        testTemplate("1213", "12{i1}3");
        testTemplate("121", "12{i1}");
        testTemplate("13", "{i1}3");
        testTemplate("a3", "{s1}3");
    }

}
