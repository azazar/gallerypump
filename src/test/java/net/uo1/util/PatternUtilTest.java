/*
 * PROPRIETARY
 */
package net.uo1.util;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Mikhail Yevchenko
 */
public class PatternUtilTest {

    public PatternUtilTest() {
    }

    @Test
    public void testGetPatternNamedGroups() {
        Set<String> groups = PatternUtil.getPatternNamedGroups(
                "Named capturing group (?<name1>regex) no\n"
                + "Named capturing group (?'name2'regex) no\n"
                + "Named capturing group (?P<name3>regex) no\n");

        assertTrue(groups.contains("name1"));
        assertTrue(groups.contains("name2"));
        assertTrue(groups.contains("name3"));
        assertEquals(3, groups.size());
    }

}
