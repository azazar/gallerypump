/*
 * PROPRIETARY/CONFIDENTIAL
 */
package net.uo1.feedreader;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import net.uo1.videofeed.entities.ExternalGallery;
import net.uo1.videofeed.entities.FeedConfiguration;

/**
 *
 * @author Mikhail Yevchenko <spam@uo1.net>
 * @since  Jun 20, 2023
 */
public class DumpSourcesTest {

    public static void main(String[] args) throws Exception {
        var sourceUrl = StreamFeedTest.class.getResource("test_feeds.json");

        try (Reader r = new InputStreamReader(sourceUrl.openStream(), StandardCharsets.UTF_8)) {
            Gson gson = new Gson();
            TypeToken<Map<String, FeedConfiguration>> typeToken = new TypeToken<Map<String, FeedConfiguration>>(){};
            
            Map<String, FeedConfiguration> rootMap = gson.fromJson(r, typeToken.getType());

            LinkedHashMap<String, Object> results = new LinkedHashMap<>();

            var threads = new ArrayList<Thread>();

            for (var sourceEntry : rootMap.entrySet()) {
                var sourceName = sourceEntry.getKey();
                var sourceConf = sourceEntry.getValue();

                var result = new LinkedHashMap<String, Object>();

                results.put(sourceName, result);

                var thread = new Thread(() -> {
                    try (var sf = new SVFeed(sourceName, sourceConf, Collections.emptySet(), 1000)) {
                        ExternalGallery gallery;

                        sf.setRowReadCallback(row -> {
                            result.put("row", row);
                        });

                        while ((gallery = sf.read()) != null) {
                            result.put("gallery", gallery);
                            result.put("validBasic", GalleryValidator.BASIC.validate(gallery, (f, m) -> true));
                            result.put("validComplete", GalleryValidator.COMPLETE.validate(gallery, (f, m) -> true));

                            if (!GalleryValidator.COMPLETE.validate(gallery, (f, m) -> true)) {
                                break;
                            }
                        }
                    }
                    catch (IOException | RuntimeException ex) {
                        result.put("error", ex.toString());
                    }
                });

                threads.add(thread);

                thread.start();
            }

            threads.forEach(t -> {
                try {
                    t.join();
                }
                catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            });

            try (var ps = new FileWriter("test_feed_download.json")) {
                ps.append(new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create().toJson(results));
            }
        }
    }
    
}
