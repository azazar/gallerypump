/*
 * PROPRIETARY
 */
package net.uo1.feedreader;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.io.IOException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.zstandard.ZstdCompressorOutputStream;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author Mikhail Yevchenko
 * @since  May 25, 2023
 */
public class TestFeedHttpServer implements AutoCloseable {

    private static final Logger LOG = Logger.getLogger(TestFeedHttpServer.class.getName());

    public static final String GZIP_CSV_URI = "/test.csv.gz";
    public static final String ZSTD_CSV_URI = "/test.csv.zst";
    public static final String ZIP_CSV_URI = "/test.zip";
    public static final String TGZ_CSV_URI = "/test.tgz";

    private Random rng = new Random();

    private final HttpServer server;
    private final int port;
    private final ExecutorService executorService;

    public TestFeedHttpServer(int port) throws IOException {
        this.port = port;
        server = HttpServer.create(new InetSocketAddress(port), 10);
        server.createContext(GZIP_CSV_URI, this::gzipCsv);
        server.createContext(ZSTD_CSV_URI, this::zstdCsv);
        server.createContext(ZIP_CSV_URI, this::zipCsv);
        server.createContext(TGZ_CSV_URI, this::tarGzipCsv);
        server.createContext("/", hx -> {
            hx.getResponseHeaders().add("Content-Type", "text/plain");
            hx.sendResponseHeaders(404, 0);
            try ( OutputStream os = hx.getResponseBody()) {
                os.write("Not Found".getBytes(StandardCharsets.ISO_8859_1));
            }
        });
        executorService = Executors.newFixedThreadPool(4);
        server.setExecutor(executorService);
        server.start();
    }

    @Override
    public void close() {
        server.stop(Integer.MAX_VALUE);
        executorService.shutdownNow();
        try {
            executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public String getUrl() {
        return "http://localhost:" + port;
    }

    public String getGzipUrl() {
        return "gz:" + getUrl() + GZIP_CSV_URI + "!test.csv";
    }

    public String getZipUrl() {
        return "zip:" + getUrl() + GZIP_CSV_URI + "!test.csv";
    }

    public String getTarGzipUrl() {
        return "tgz:" + getUrl() + GZIP_CSV_URI + "!test.csv";
    }

    private void gzipCsv(HttpExchange hx) throws IOException {
        hx.getResponseHeaders().add("Content-Type", "application/gzip");
        hx.sendResponseHeaders(200, 0);

        try ( Writer sw = new OutputStreamWriter(new GZIPOutputStream(hx.getResponseBody()))) {
            buildCsv(sw);
        } catch (IOException ex) {
            Logger.getLogger(TestFeedHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void zstdCsv(HttpExchange hx) throws IOException {
        hx.getResponseHeaders().add("Content-Type", "application/gzip");
        hx.sendResponseHeaders(200, 0);

        try ( Writer sw = new OutputStreamWriter(new ZstdCompressorOutputStream(hx.getResponseBody()))) {
            buildCsv(sw);
        } catch (IOException ex) {
            Logger.getLogger(TestFeedHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void zipCsv(HttpExchange hx) throws IOException {
        hx.getResponseHeaders().add("Content-Type", "application/zip");
        hx.sendResponseHeaders(200, 0);

        try ( ZipOutputStream zos = new ZipOutputStream(hx.getResponseBody())) {
            zos.putNextEntry(new ZipEntry("test.csv"));
            Writer sw = new OutputStreamWriter(zos);
            buildCsv(sw);
            sw.flush();
            zos.closeEntry();
        } catch (IOException ex) {
            Logger.getLogger(TestFeedHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void tarGzipCsv(HttpExchange hx) throws IOException {
        hx.getResponseHeaders().add("Content-Type", "application/gzip");
        hx.sendResponseHeaders(200, 0);

        try ( TarArchiveOutputStream tos = new TarArchiveOutputStream(new GZIPOutputStream(hx.getResponseBody()))) {
            byte[] csv = buildCsv();

            TarArchiveEntry entry = new TarArchiveEntry("test.csv");
            entry.setSize(csv.length);

            tos.putArchiveEntry(entry);
            tos.write(csv);
            tos.closeArchiveEntry();
        } catch (IOException ex) {
            Logger.getLogger(TestFeedHttpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public byte[] buildCsv() throws IOException {
        ByteArrayOutputStream o = new ByteArrayOutputStream();

        try ( OutputStreamWriter w = new OutputStreamWriter(o)) {
            buildCsv(w);
        }

        return o.toByteArray();
    }

    public void buildCsv(Writer sw) throws IOException {
        String[] fields = "source,source_id,url,title,seconds,thumbs,embed,categories,tags,channels,models,date,custom".split(",");

        sw.append(String.join(",", fields) + "\n");

        for (int i = 0; i < 1000; i++) {
            int f = 0;
            for (String field : fields) {
                if (f++ > 0) {
                    sw.append(',');
                }

                switch (field) {
                    case "source":
                        sw.append("test");
                        break;
                    case "source_id":
                        sw.append(String.valueOf(1 + i));
                        break;
                    case "url":
                        sw.append("http://test/" + RandomStringUtils.randomAlphanumeric(10, 40));
                        break;
                    case "title":
                        sw.append(RandomStringUtils.randomAlphanumeric(10, 40));
                        break;
                    case "seconds":
                        sw.append(String.valueOf(rng.nextInt(30, 3000) + 30));
                        break;
                    case "thumbs":
                        sw.append("http://test/" + RandomStringUtils.randomAlphanumeric(10, 40));
                        sw.append(';');
                        sw.append("http://test/" + RandomStringUtils.randomAlphanumeric(10, 40));
                        break;
                    case "embed":
                        sw.append("<iframe src=\"" + RandomStringUtils.randomAlphanumeric(10, 40) + "\"></iframe>");
                        break;
                    case "categories":
                    case "tags":
                    case "channels":
                        sw.append(RandomStringUtils.randomAlphanumeric(3, 8) + ";" + RandomStringUtils.randomAlphanumeric(3, 8));
                        break;
                    case "models":
                        sw.append(RandomStringUtils.randomAlphanumeric(3, 8) + " " + RandomStringUtils.randomAlphanumeric(3, 8));
                        break;
                    case "date":
                        sw.append(RandomStringUtils.randomAlphanumeric(10, 40));
                        break;
                    case "custom":
                        sw.append(RandomStringUtils.randomAlphanumeric(10, 40));
                        break;
                }
            }
            sw.append("\n");
        }
    }

}
