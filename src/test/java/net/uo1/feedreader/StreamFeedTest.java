/*
 * PROPRIETARY
 */
package net.uo1.feedreader;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.uo1.videofeed.entities.ExternalGallery;
import net.uo1.videofeed.entities.FeedConfiguration;

/**
 *
 * @author Mikhail Yevchenko
 */
public class StreamFeedTest {

    static final int PORT = 7777;

    private static TestFeedHttpServer server;

    @BeforeAll
    public static void startServer() throws IOException {
        server = new TestFeedHttpServer(PORT);
    }

    @AfterAll
    public static void stopServer() {
        server.close();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        try ( TestFeedHttpServer srv = new TestFeedHttpServer(7777)) {
            System.in.read();
        }
    }

    @ParameterizedTest
    @ValueSource(
            strings = {
                "gz:http://server/test.csv.gz!test.csv",
                "zst:http://server/test.csv.zst!test.csv",
                "zip:http://server/test.zip!test.csv",
                "tgz:http://server/test.tgz!test.csv",}
    )
    public void testIntegral(String url) throws IOException {
        var conf = new FeedConfiguration();

        conf.setUrl(url.replace("http://server", server.getUrl()));

        try (StreamFeed sf = new SVFeed("ts", conf, Collections.emptySet(), Integer.MAX_VALUE)) {
            for (int i = 0; i < 1000; i++) {
                ExternalGallery gi = sf.read();
    
                assertNotNull(gi);
                assertFalse(gi.thumbs[0].contains(conf.getListDelimiter()));
            }
        }
    }

    @TestFactory
    public List<DynamicTest> testPredefined() throws Exception {
        var tests = new ArrayList<DynamicTest>();
        var sourceUrl = StreamFeedTest.class.getResource("test_feeds.json");

        try (Reader r = new InputStreamReader(sourceUrl.openStream(), StandardCharsets.UTF_8)) {
            Gson gson = new Gson();
            TypeToken<Map<String, FeedConfiguration>> typeToken = new TypeToken<Map<String, FeedConfiguration>>(){};
            
            Map<String, FeedConfiguration> rootMap = gson.fromJson(r, typeToken.getType());

            for (var sourceEntry : rootMap.entrySet()) {
                var sourceName = sourceEntry.getKey();
                var sourceConf = sourceEntry.getValue();

                var testUri = URI.create(sourceUrl.toExternalForm() + "#" + sourceName);

                tests.add(DynamicTest.dynamicTest(sourceConf.getSourceName() + " (" + sourceName + ')', testUri, () -> {
                    assertNotNull(sourceConf.getUrl(), "null url");

                    try (var sf = new SVFeed(sourceName, sourceConf, Collections.emptySet(), 100)) {
                        ExternalGallery gallery;

                        int index = 0;

                        while ((gallery = sf.read()) != null) {
                            assertNotNull(gallery);
    
                            var valid = GalleryValidator.BASIC.validate(gallery, (f, m) -> true);
    
                            assertTrue(valid, "bad gallery: " + gallery);
                            
                            index++;
                        }

                        assertTrue(index > 0, "no galleries read");
                    }
                }));
            }
        }

        return tests;
    }

}
