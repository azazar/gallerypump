/*
 * PROPRIETARY/CONFIDENTIAL
 */
package net.uo1.feedreader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;
import net.uo1.videofeed.entities.FeedConfiguration;
import net.uo1.videofeed.entities.ExternalGallery;

@SuppressWarnings("unused")
public class SVFeedExample {
    
    public static void main(String[] args) throws IOException {
        var sourceUrl = new URL("https://gitlab.com/azazar/gallerypump/-/raw/master/src/test/resources/net/uo1/feedreader/test_feeds.json");

        try (Reader r = new InputStreamReader(sourceUrl.openStream(), StandardCharsets.UTF_8)) {
            Gson gson = new Gson();
            TypeToken<Map<String, FeedConfiguration>> typeToken = new TypeToken<Map<String, FeedConfiguration>>(){};
            
            Map<String, FeedConfiguration> rootMap = gson.fromJson(r, typeToken.getType());

            for (var sourceEntry : rootMap.entrySet()) {
                var sourceName = sourceEntry.getKey();
                var feedConfiguration = sourceEntry.getValue();
                
                try (var feed = new SVFeed(sourceName, feedConfiguration, Set.of("title_ru"), 50_000_000)) {
                    ExternalGallery gallery;

                    while ((gallery = feed.read()) != null) {
                        // Do something with gallery
                    }
                } catch (IOException e) {
                    System.err.println("Error loaded feed from " + feedConfiguration.getUrl() + ": " + e.getMessage());
                }
            }
        } catch (IOException e) {
            System.err.println("Error loading configuration file: " + e.getMessage());
        }
    }
}
