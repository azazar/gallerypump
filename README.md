# Gallery Pump

This library facilitates importing gallery collections from feed dumps.

## Configuration

The configuration is read from a Map and it is recommended to use Gson for the convenient storage and loading of configurations. Although Gallery Pump does not directly depend on Gson, it offers a convenient method to load the library's configuration. The `FeedConfiguration` class is a POJO compatible with Gson's object deserialization.

### Example

This concise example reads the feed configuration from a JSON file and iterates over feed entries:

```java
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;
import net.uo1.videofeed.entities.FeedConfiguration;
import net.uo1.videofeed.entities.ExternalGallery;

public class SVFeedExample {
    
    public static void main(String[] args) throws IOException {
        var sourceUrl = new URL("https://gitlab.com/azazar/gallerypump/-/raw/master/src/test/resources/net/uo1/feedreader/test_feeds.json");

        try (Reader r = new InputStreamReader(sourceUrl.openStream(), StandardCharsets.UTF_8)) {
            Gson gson = new Gson();
            TypeToken<Map<String, FeedConfiguration>> typeToken = new TypeToken<Map<String, FeedConfiguration>>(){};
            
            Map<String, FeedConfiguration> rootMap = gson.fromJson(r, typeToken.getType());

            for (var sourceEntry : rootMap.entrySet()) {
                var sourceName = sourceEntry.getKey();
                var feedConfiguration = sourceEntry.getValue();
                
                try (var feed = new SVFeed(sourceName, feedConfiguration, Set.of("title_ru"), 50_000_000)) {
                    ExternalGallery gallery;

                    while ((gallery = feed.read()) != null) {
                        // Do something with gallery
                    }
                } catch (IOException e) {
                    System.err.println("Error loaded feed from " + feedConfiguration.getUrl() + ": " + e.getMessage());
                }
            }
        } catch (IOException e) {
            System.err.println("Error loading configuration file: " + e.getMessage());
        }
    }
}
```

## Supported Fields

- source
- source_id
- url
- title
- description
- seconds
- preview
- thumbs
- embed
- orientation
- categories
- tags
- channels
- models
- studios
- date
- quality

## Usage with Maven

Add the repository:

```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

Add the dependency:

```xml
<dependency>
    <groupId>com.gitlab.azazar</groupId>
    <artifactId>gallerypump</artifactId>
    <version>1.0.17</version>
</dependency>
```

Add Gson dependency if necessary:

```xml
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.10.1</version>
</dependency>
```
